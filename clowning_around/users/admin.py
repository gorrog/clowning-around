from django.contrib import admin
from django.contrib.auth import admin as auth_admin
from django.contrib.auth import get_user_model

from clowning_around.users.forms import UserChangeForm, UserCreationForm
from clowning_around.users.models import Client, Clown, Troupe, TroupeLeader

User = get_user_model()


@admin.register(User)
class UserAdmin(auth_admin.UserAdmin):

    form = UserChangeForm
    add_form = UserCreationForm
    fieldsets = (
        ("User", {"fields": ("name", "profile_type")}),
    ) + auth_admin.UserAdmin.fieldsets
    list_display = ["username", "name", "is_superuser"]
    search_fields = ["name"]


admin.site.register(Client)
admin.site.register(TroupeLeader)
admin.site.register(Troupe)
admin.site.register(Clown)
