from django.contrib.auth.models import (
    AbstractBaseUser,
    AbstractUser,
    Permission,
    PermissionsMixin,
)
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _


class User(AbstractUser):
    CLIENT = "CT"
    TROUPELEADER = "TR"
    CLOWN = "CN"

    USER_TYPES_CHOICES = [
        (CLIENT, "Client"),
        (TROUPELEADER, "TroupeLeader"),
        (CLOWN, "Clown"),
    ]

    # First Name and Last Name do not cover name patterns
    # around the globe.
    name = models.CharField(_("Name of User"), blank=True, max_length=255)

    profile_type = models.CharField(max_length=2, choices=USER_TYPES_CHOICES)

    def get_absolute_url(self):
        return reverse("users:detail", kwargs={"username": self.username})

    def get_profile_object(self):
        return getattr(self, self.get_profile_type_display().lower())


class Client(models.Model):
    """
    Served by clowns at a particular address
    Can view upcoming and past appointments
    Can rate past appointments
    """

    user = models.OneToOneField(to=User, on_delete=models.CASCADE, related_name="client")
    address = models.CharField(max_length=255)
    phone_number = models.CharField(max_length=50, null=True, blank=True)

    @staticmethod
    def get_allowed_permissions():
        allowed_permissions = [
            Permission.objects.get(codename="view_appointment"),
            Permission.objects.get(codename="change_appointment"),
        ]
        return allowed_permissions

    def save(self, *args, **kwargs):
        # Only run if this is a new record
        if self._state.adding:
            super().save(*args, **kwargs)  # Call the "real" save() method.
            ALLOWED_PERMISSIONS = [
                Permission.objects.get(codename="view_appointment"),
                Permission.objects.get(codename="change_appointment"),
            ]
            self.user.user_permissions.add(*self.get_allowed_permissions())

    class Meta:
        verbose_name = "Client"
        verbose_name_plural = "Clients"


class TroupeLeader(models.Model):
    """
    Leader of a troup.
    Able to add, view, change, and delete appointments
    """

    user = models.OneToOneField(
        to=User, on_delete=models.CASCADE, related_name="troupe_leader"
    )

    @staticmethod
    def get_allowed_permissions():
        allowed_permissions = [
            Permission.objects.get(codename="add_appointment"),
            Permission.objects.get(codename="view_appointment"),
            Permission.objects.get(codename="change_appointment"),
            Permission.objects.get(codename="delete_appointment"),
            Permission.objects.get(codename="view_issue"),
            Permission.objects.get(codename="change_issue"),
            Permission.objects.get(codename="delete_issue"),
            Permission.objects.get(codename="view_contactrequest"),
            Permission.objects.get(codename="change_contactrequest"),
            Permission.objects.get(codename="delete_contactrequest"),
        ]
        return allowed_permissions

    def save(self, *args, **kwargs):
        # Only run if this is a new record
        if self._state.adding:
            super().save(*args, **kwargs)  # Call the "real" save() method.
            self.user.user_permissions.add(*self.get_allowed_permissions())

    class Meta:
        verbose_name = "Troupe Leader"
        verbose_name_plural = "Troupe Leaders"


class Troupe(models.Model):
    """
    A group of clowns, headed up by a troupe leader
    """

    troupe_leader = models.ForeignKey(to=TroupeLeader, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.troupe_leader}'s troupe"

    class Meta:
        verbose_name = "Troupe"
        verbose_name_plural = "Troupe"


class Clown(models.Model):
    """
    Member of a troupe, headed by a troupe leader.
    Can view appointments and update their statuses
    Can raise issues with an appointment
    Can request client contact details
    """

    user = models.OneToOneField(to=User, on_delete=models.CASCADE, related_name="clown")
    troupe = models.ForeignKey(
        to=Troupe, on_delete=models.SET_NULL, related_name="clowns", null=True, blank=True
    )

    @staticmethod
    def get_allowed_permissions():
        allowed_permissions = [
            Permission.objects.get(codename="view_appointment"),
            Permission.objects.get(codename="change_appointment"),
            Permission.objects.get(codename="add_issue"),
            Permission.objects.get(codename="view_issue"),
            Permission.objects.get(codename="change_issue"),
            Permission.objects.get(codename="add_contactrequest"),
            Permission.objects.get(codename="view_contactrequest"),
            Permission.objects.get(codename="change_contactrequest"),
        ]
        return allowed_permissions

    def save(self, *args, **kwargs):
        # Only run if this is a new record
        if self._state.adding:
            super().save(*args, **kwargs)  # Call the "real" save() method.
            self.user.user_permissions.add(*self.get_allowed_permissions())

    class Meta:
        verbose_name = "Clown"
        verbose_name_plural = "Clowns"
