import pytest
from django.conf import settings

from clowning_around.users.models import Client, Clown, TroupeLeader
from clowning_around.users.tests.factories import (
    ClientFactory,
    ClownFactory,
    TroupeLeaderFactory,
)

pytestmark = pytest.mark.django_db


def test_user_get_absolute_url(user: settings.AUTH_USER_MODEL):
    assert user.get_absolute_url() == f"/users/{user.username}/"


def test_client_permissions_created():
    client = ClientFactory()
    expected_permissions_set = set(Client.get_allowed_permissions())
    actual_permissions_set = set(client.user.user_permissions.all())
    assert expected_permissions_set == actual_permissions_set


def test_troupe_leader_permissions_created():
    troupe_leader = TroupeLeaderFactory()
    expected_permissions_set = set(TroupeLeader.get_allowed_permissions())
    actual_permissions_set = set(troupe_leader.user.user_permissions.all())
    assert expected_permissions_set == actual_permissions_set


def test_clown_permissions_created():
    clown = ClownFactory()
    expected_permissions_set = set(Clown.get_allowed_permissions())
    actual_permissions_set = set(clown.user.user_permissions.all())
    assert expected_permissions_set == actual_permissions_set
