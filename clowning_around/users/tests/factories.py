import random
from typing import Any, Sequence

import factory
from django.contrib.auth import get_user_model
from factory import DjangoModelFactory, Faker, post_generation

from clowning_around.users.models import Client, Clown, TroupeLeader, User, Troupe


class UserFactory(DjangoModelFactory):

    username = Faker("user_name")
    email = Faker("email")
    name = Faker("name")
    profile_type = User.USER_TYPES_CHOICES[random.randint(0, 2)][0]

    @post_generation
    def password(self, create: bool, extracted: Sequence[Any], **kwargs):
        password = extracted
        if not password:
            password = Faker(
                "password",
                length=42,
                special_chars=True,
                digits=True,
                upper_case=True,
                lower_case=True,
            ).generate(extra_kwargs={})
        self.set_password(password)

    class Meta:
        model = get_user_model()
        django_get_or_create = ["username"]


class ClientFactory(DjangoModelFactory):

    user = factory.SubFactory(UserFactory)
    address = Faker("address")
    phone_number = Faker("phone_number")

    class Meta:
        model = Client

class TroupeLeaderFactory(DjangoModelFactory):

    user = factory.SubFactory(UserFactory)

    class Meta:
        model = TroupeLeader

class TroupeFactory(DjangoModelFactory):

    troupe_leader = factory.SubFactory(TroupeLeaderFactory)

    class Meta:
        model = Troupe

class ClownFactory(DjangoModelFactory):

    user = factory.SubFactory(UserFactory)

    class Meta:
        model = Clown
