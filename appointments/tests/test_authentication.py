import unittest
import pytest
import json

from morelia import verify
from clowning_around.users.tests.factories import UserFactory, ClientFactory, ClownFactory, TroupeLeaderFactory
from appointments.tests.test_helpers import parse_text_table
from clowning_around.users.models import User, Client, TroupeLeader, Clown

from django.test import Client as TestClient

pytestmark = pytest.mark.django_db

class AuthenticationTestCase(unittest.TestCase):

    def setUp(self):
        self.client = TestClient()

    def tearDownScenario(self):
        User.objects.all().delete()
        Client.objects.all().delete()
        TroupeLeader.objects.all().delete()
        Clown.objects.all().delete()

    def test_login(self):
        """ Login feature """
        verify('appointments/features/authentication.feature', self)

    def step_that_the_login_page_is_located_at_accounts_login(self, login_url):
        r'that the login page is located at "([^"]+)"'
        self.login_url = login_url

    def step_that_there_are_registerd_users_with_the_data_below(self, _text):
        r'that there are registerd users with the data below'
        user_data = parse_text_table(_text)
        for user in user_data:
            if user['profile_type'] == "CLIENT":
                user = UserFactory.build(username=user['username'], password=user['password'], profile_type=User.CLIENT)
                user.save()
                client = ClientFactory.build(user=user)
                client.save()
            elif user['profile_type'] == "TROUPELEADER":
                user = UserFactory.build(username=user['username'], password=user['password'], profile_type=User.TROUPELEADER)
                user.save()
                troupe_leader = TroupeLeaderFactory.build(user=user)
                troupe_leader.save()
            elif user['profile_type'] == "CLOWN":
                user = UserFactory.build(username=user['username'], password=user['password'], profile_type=User.CLOWN)
                user.save()
                clown = ClownFactory.build(user=user)
                clown.save()
            else:
                raise NotImplementedError(f"profile_type {user['profile_type']} is not valid")

    def step_I_make_a_POST_request_to_the_login_page_with_user_name_username_and_password_password(self, username, password):
        r'I make a POST request to the login page with user name (.+) and password (.+)'
        auth_data = {
            "username": username,
            "password": password
        }
        self.response = self.client.post(
            path=self.login_url,
            data=auth_data,
            content_type="application/json",
        )

    def step_I_will_receive_a_number_response_code(self, number):
        r'I will receive a "([^"]+)" response code'
        self.assertEqual(number, str(self.response.status_code))

    def step_I_will_recieve_a_JSON_payload(self):
        r'I will recieve a JSON payload'
        self.json_payload = json.loads(self.response.content)

    def step_the_payload_will_contain_a_key_key_with_a_number_digit_string(self, key, number):
        r'the payload will contain a key "([^"]+)", with a "([^"]+)" digit string'
        self.assertTrue(key in self.json_payload)
        self.assertEqual(str, type(self.json_payload[key]))
        self.assertEqual(int(number), len(self.json_payload[key]))
