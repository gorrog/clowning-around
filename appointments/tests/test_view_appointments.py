import unittest
import pytest
import json

from morelia import verify
from clowning_around.users.tests.factories import UserFactory, ClientFactory, ClownFactory, TroupeLeaderFactory, TroupeFactory
from appointments.tests.test_helpers import parse_text_table
from clowning_around.users.models import User, Client, TroupeLeader, Clown, Troupe
from appointments.models import Appointment
from datetime import datetime, timedelta

from django.test import Client as TestClient

pytestmark = pytest.mark.django_db

class ViewAppointmentsTestCase(unittest.TestCase):

    def setUp(self):
        self.client = TestClient()
        self.two_days_ago = datetime.now() - timedelta(days=2)
        self.yesterday = datetime.now() - timedelta(days=1)

    def tearDownScenario(self):
        User.objects.all().delete()
        Client.objects.all().delete()
        TroupeLeader.objects.all().delete()
        Clown.objects.all().delete()
        Appointment.objects.all().delete()

    def test_login(self):
        """ Login feature """
        verify('appointments/features/view_appointments.feature', self)

    def step_that_the_login_page_is_located_at_rest_auth_login(self, url):
        r'that the login page is located at "([^"]+)"'
        self.login_url = url

    def step_the_view_appointments_page_is_located_at_appointments(self, url):
        r'the view appointments page is located at "([^"]+)"'
        self.appointments_url = url

    def step_that_there_are_registerd_users_with_the_data_below(self, _text):
        r'that there are registerd users with the data below'
        # Save this to self for later use in logging in
        self.user_data = parse_text_table(_text)
        for user in self.user_data:
            if user['profile_type'] == "CLIENT":
                user = UserFactory.build(username=user['username'], password=user['password'], profile_type=User.CLIENT)
                user.save()
                client = ClientFactory.build(user=user)
                client.save()
            elif user['profile_type'] == "TROUPELEADER":
                user = UserFactory.build(username=user['username'], password=user['password'], profile_type=User.TROUPELEADER)
                user.save()
                troupe_leader = TroupeLeaderFactory.build(user=user)
                troupe_leader.save()
            elif user['profile_type'] == "CLOWN":
                user = UserFactory.build(username=user['username'], password=user['password'], profile_type=User.CLOWN)
                user.save()
                clown = ClownFactory.build(user=user)
                clown.save()
            else:
                raise NotImplementedError(f"profile_type {user['profile_type']} is not valid")

    def step_that_these_troupes_are_in_the_database_already(self, _text):
        r'that these troupes are in the database already'
        troupe_data = parse_text_table(_text)
        for t in troupe_data:
            troupe_leader_user = User.objects.get(username=t['troupe_leader'])
            troupe_leader = TroupeLeader.objects.get(user=troupe_leader_user)
            clown_users = User.objects.filter(username__in=t['clowns'])
            clowns = Clown.objects.filter(user__in=clown_users)
            troupe = TroupeFactory.build(troupe_leader=troupe_leader)
            troupe.save()
            for clown in clowns:
                clown.troupe = troupe
                clown.save()

    def step_that_the_following_appointments_are_in_the_database_already(self, _text):
        r'that the following appointments are in the database already'

        appointment_data = parse_text_table(_text, self)
        for appointment in appointment_data:
            client_username = appointment['client']
            appointment['client'] = Client.objects.get(user__username=client_username)

            clown_username = appointment['clown']
            appointment['clown'] = Clown.objects.get(user__username=clown_username)

            Appointment.objects.create(**appointment)

    def step_that_every_user_has_logged_in_and_received_their_key_which_they_will_use_for_all_requests(self):
        r'that every user has logged in and received their key which they will use for all requests'

        user_list = []
        for user in self.user_data:
            auth_data = {
                "username": user["username"],
                "password": user["password"]
            }
            self.response = self.client.post(
                path=self.login_url,
                data=auth_data,
                content_type="application/json",
            )
            json_payload = json.loads(self.response.content)
            user['key'] = json_payload['key']
            user_list.append(user)

    def step_a_client_with_username_username_makes_a_GET_request_to_the_appointments_page(self, username):
        r'a client with username (.+) makes a GET request to the appointments page'

        raise NotImplementedError('a client with username <username> makes a GET request to the appointments page')

    def step_the_page_will_respond_with_a_response_code_code(self, response_code):
        r'the page will respond with a (.+) code'

        raise NotImplementedError('the page will respond with a <response_code> code')

    def step_there_will_be_number_of_results_appointments_returned(self, number_of_results):
        r'there will be (.+) appointments returned'

        raise NotImplementedError('there will be <number_of_results> appointments returned')
