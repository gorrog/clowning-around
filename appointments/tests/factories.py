import random
from typing import Any, Sequence

import factory
from appointments.models import Appointment, ContactRequest, Issue
from django.contrib.auth import get_user_model
from factory import DjangoModelFactory, Faker, post_generation

from clowning_around.users.models import Client, Clown, TroupeLeader, User
from clowning_around.users.tests.factories import (
    ClientFactory,
    ClownFactory,
    TroupeLeaderFactory,
)


class AppointmentFactory(DjangoModelFactory):
    appointment_date = Faker("date_time")
    rating = Appointment.RATING_CHOICES[random.randint(0, 4)][0]
    status = Appointment.STATUS_CHOICES[random.randint(0, 3)][0]
    description = Faker("paragraph")
    client = factory.SubFactory(ClientFactory)
    clown = factory.SubFactory(ClownFactory)

    class Meta:
        model = Appointment


class IssueFactory(DjangoModelFactory):

    title = Faker("text", max_nb_chars=50)
    description = Faker("paragraph")
    appointment = factory.SubFactory(AppointmentFactory)

    class Meta:
        model = Issue


class ContactRequestFactory(DjangoModelFactory):

    for_client = factory.SubFactory(ClientFactory)
    by_clown = factory.SubFactory(ClownFactory)
    reason = Faker("paragraph")

    class Meta:
        model = ContactRequest
