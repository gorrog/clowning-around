from astropy.io import ascii

import ast
import numpy

# Utils for BDD (Morelia)
# TODO: Extract the table parsing logic from astropy
# and create a PR into morelia with this logic and the functions below


def is_var(text, wrapped_in="<>"):
    """
    Checks whether supplied text is wrapped inside 2 wrapped_in characters.
    Returns True with the containing text if it is, False and original if not.
    """
    if text[0] == wrapped_in[0] and text[-1] == wrapped_in[1]:
        return True, text[1:-1]
    return False, text


def pythonize(expression, variable_context):
    """
    Converts an expression to a Python type if possible.

    First attempts to convert numpy types to native Python equivalents.
    Then, it will evaluate the expression and return a Python data type.
    If this fails and the string looks like a variable, return the attribute
    in the variable context with the string's name
    Returns a string if this fails.
    """
    if isinstance(expression, numpy.generic):
        expression = numpy.asscalar(expression)
    try:
        return ast.literal_eval(expression)
    except Exception:
        if variable_context and type(expression) == str:
            var, stripped = is_var(expression)
            if var:
                return getattr(variable_context, stripped)
        return expression


def parse_text_table(ascii_table, variable_context=None):
    """
    Parse an ASCII formatted table and return a list of dicts - one per row.

    Uses the astropy package's 'io.ascii' module to get a data table from  an
    ASCII based one.

    Given the following table:

    | name    | favourite fruit | favourite vegetable |

    | Bob     | apples          | cucumber            |
    | Belinda | oranges         | lettuce             |
    | Baz     | fish            | fish                |

    This function will return the following list of dictionaries:
    [
    {"name": "Bob",
    "favourite fruit": "apples",
    "favourite vegetable": "cucumber" }
    {"name": "Belinda",
    "favourite fruit": "oranges",
    "favourite vegetable": "lettuce" }
    {"name": "Baz",
    "favourite fruit": "fish",
    "favourite vegetable": "fish" }
    ]
    """
    table = ascii.read(ascii_table, format="fixed_width", header_start=0)
    list_of_dicts = []
    for row in range(len(table)):
        rowdict = {}
        for column in table.colnames:
            value = pythonize(table.columns[column][row], variable_context)
            rowdict[column] = value
        list_of_dicts.append(rowdict)
    return list_of_dicts
