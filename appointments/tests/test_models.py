import pytest
from appointments.tests.factories import (
    AppointmentFactory,
    ContactRequestFactory,
    IssueFactory,
)
from django.conf import settings

pytestmark = pytest.mark.django_db


def test_appointment_str():
    appointment = AppointmentFactory()

    client_str = str(appointment.client)
    date_str = str(appointment.appointment_date)
    clown_str = str(appointment.clown)

    expected_str = f"Client {client_str} at {date_str} by clown {clown_str}"
    actual_str = str(appointment)
    assert expected_str == actual_str


def test_issue_str():
    issue = IssueFactory()

    title_str = str(issue.title)
    appointment_str = str(issue.appointment)

    expected_str = f"{title_str} for {appointment_str}"
    actual_str = str(issue)
    assert expected_str == actual_str


def test_contact_request_str():
    contact_request = ContactRequestFactory()

    for_client_str = str(contact_request.for_client)
    by_clown_str = str(contact_request.by_clown)

    expected_str = f"For client {for_client_str} by clown {by_clown_str}"
    actual_str = str(contact_request)
    assert expected_str == actual_str
