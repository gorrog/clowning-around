from appointments.models import Appointment, ContactRequest, Issue
from django.contrib import admin

# Register your models here.

admin.site.register(Appointment)
admin.site.register(Issue)
admin.site.register(ContactRequest)
