Feature: Authentication
  In order to access the 'clowing around' appointments system
  as a Client, Clown or Troupe Leader
  I should be able to log in and have permissions appropriate to my role

Background:
  Given that the login page is located at "/rest-auth/login/"
  And that there are registerd users with the data below
    """
    | profile_type | username    | password        |

    | CLIENT       | mr_nice_guy | abc123          |
    | TROUPELEADER | mr_boss_man | theBigChee5e    |
    | CLOWN        | bozo123     | somethingFunny! |
    """


Scenario: Successfully log in
  When I make a POST request to the login page with user name <username> and password <password>
  Then I will receive a "200" response code
  And I will recieve a JSON payload
  And the payload will contain a key "key", with a "40" digit string

    | username    | password        |

    | mr_nice_guy | abc123          |
    | mr_boss_man | theBigChee5e    |
    | bozo123     | somethingFunny! |
