Feature: View Appointments
  In order to know what is upcoming and what has passed
  as a Client, Clown or Troupe Leader
  I should be able to view appointments related to me

Background:
  Given that the login page is located at "/rest-auth/login/"
  And the view appointments page is located at "/appointments/"
  And that there are registerd users with the data below
    """
    | profile_type | username       | password        |

    | CLIENT       | mr_nice_guy    | abc123          |
    | TROUPELEADER | mr_boss_man    | theBigChee5e    |
    | TROUPELEADER | mrs_boss_woman | youDoWhatISay   |
    | CLOWN        | bozo123        | somethingFunny! |
    | CLOWN        | fredfred       | hardeehar       |
    | CLOWN        | ronald         | ImlovingIT      |
    | CLIENT       | mrs_fussy      | nothing_right   |
    | CLIENT       | mr_flakey      | save_our_money  |
    """

   And that these troupes are in the database already
     """
     | troupe_leader  | clowns                  |

     | mr_boss_man    | ['bozo123', 'fredfred'] |
     | mrs_boss_woman | ['ronald']              |
     """

   And that the following appointments are in the database already
     """
     | appointment_date | status | description                         | client      | clown   |

     | <two_days_ago>   | CCD    | Entertain 30 kids with magic tricks | mr_nice_guy | bozo123 |
     | <yesterday>      | CPT    | Birthday party for twins            | mr_nice_guy | ronald  |
     | <yesterday>      | CPT    | Halloween party for twins           | mrs_fussy   | bozo123 |
     """

   And that every user has logged in and received their key which they will use for all requests


Scenario: 1 - View appointments as a client
  When a client with username <username> makes a GET request to the appointments page
  Then the page will respond with a <response_code> code
  And there will be <number_of_results> appointments returned

    | username    | response_code | number_of_results |

    | mr_nice_guy |           200 |                 2 |
    | mrs_fussy   |           200 |                 1 |
    | mr_flakey   |           404 |                 0 |
